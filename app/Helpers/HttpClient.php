<?php

namespace App\Helpers;

/**
 * Description of HttpClient
 *
 * @author Angga Bayu S
 * 
 * 
 */
class HttpClient
{

    public function postRequest($url, $data, $token = null) {
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => config("app.API_TIMEOUT"),
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTREDIR => 3,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "Token: $token"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            header("content-type: application/json");
            return json_encode(['status'=> 0, 'error' => $err]);
        } else {
            header("content-type: application/json");
            return $response;
        }
    }
    
    public function putRequest($url, $data, $token = null) {
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => config("app.API_TIMEOUT"),
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTREDIR => 3,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "Token: $token"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            header("content-type: application/json");
            return json_encode(['status'=> 0, 'error' => $err]);
        } else {
            header("content-type: application/json");
            return $response;
        }
    }

    public function getRequest($url, $token = null) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => config("app.API_TIMEOUT"),
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "token: $token"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            header("content-type: application/json");
            return json_encode(['status'=> 0, 'error' => $err]);
        } else {
            return $response;
        }
    }

    public function upload($url, $token, $fileName) {

        $imageFullPath = $this->_uploadToServer($fileName);
        if ($imageFullPath == null) {
            exit("Error occured when uploading file $fileName");
        }
        print $imageFullPath;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => config("app.API_TIMEOUT"),
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; "
            . "name=\"image\"; filename=@\"" . realpath($imageFullPath) . "\"\r\nContent-Type: image/png\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"Token\"\r\n\r\n$token\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "token: $token"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    private function _uploadToServer($fileName) {
        $target_dir = config("app.APP_TEMP_FILE_UPLOAD");

        $target_file = $target_dir . basename(str_replace(' ', '_', $_FILES['' . $fileName . '']["name"]) . "__" . uniqid() . ".png");
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if (isset($_POST)) {
            $check = getimagesize($_FILES['' . $fileName . '']["tmp_name"]);
            if ($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $uploadOk = 0;
        }

        if ($_FILES['' . $fileName . '']["size"] > 500000) {
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        } else {
            if (move_uploaded_file($_FILES['' . $fileName . '']["tmp_name"], $target_file)) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }

        if ($uploadOk == 1) {
            return $target_file;
        } else {
            return null;
        }
    }
    
    public static function forceRedirect($url) {
        print "<html><body onload=\"self.location.href='$url'\"></body></html>";
        exit(0);
    }

}

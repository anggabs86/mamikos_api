<?php

namespace App\Helpers;

/**
 * Description of SecurityHelper
 *
 * @author Angga Bayu S
 */
class SecurityHelper
{

    /**
     * Encrypt $string 
     * 
     * @param String $string
     * @return String
     */
    public static function rsaEncrypt($string, $key)
    {
        $encrypted = null;
        openssl_public_encrypt($string, $encrypted, $key);

        return base64_encode($encrypted);
    }

    /**
     * Descrypt encrypted data
     * 
     * @param String $string
     * @return String
     */
    public static function rsaDecrypt($string, $key)
    {
        $decrypted = null;
        $stringDecode = base64_decode($string);

        openssl_private_decrypt($stringDecode, $decrypted, $key);

        return $decrypted;
    }

    /**
     * Return public key
     * 
     * @return String
     * @throws \Exception
     */
    public static function getPublicKey()
    {
        if (!file_exists(env('RSA_PUBLIC_KEY_PATH'))) {
            throw new \Exception(env('RSA_PUBLIC_KEY_PATH') . ' does not exist!');
        }

        return file_get_contents(env('RSA_PUBLIC_KEY_PATH'));
    }

    /**
     * Return private key
     * 
     * @return String
     * @throws \Exception
     */
    public static function getPrivateKey()
    {
        if (!file_exists(env('RSA_PRIVATE_KEY_PATH'))) {
            throw new \Exception(env('RSA_PRIVATE_KEY_PATH') . ' does not exist!');
        }

        return file_get_contents(env('RSA_PRIVATE_KEY_PATH'));
    }

}

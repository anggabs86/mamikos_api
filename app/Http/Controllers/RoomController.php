<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Room;
use App\Model\RoomDetail;
use App\Model\User;
use App\Model\UserCredit;
use DB;

/**
 * Class RoomController for handling Rooms 
 * 
 * @author Angga Bayu S<anggabs86@gmail.com>
 */
class RoomController extends Controller
{

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        parent::__construct();
    }

    /**
     * Add new room
     * 
     * @param Request $request
     * @return JSON
     */
    public function add(Request $request)
    {
        //Validate input
        $this->validate($request, [
            'owner_id' => 'required|max:11|exists:users,id',
            'city_id' => 'required|max:11|integer|exists:city,id',
            'title' => 'required',
            'price' => 'required',
            'user_id' => 'required|max:11|exists:users,id',
            'facility' => 'required',
            'description' => 'required',
            'available_room' => 'required|integer',
            'rate' => 'required|integer',
        ]);

        //Check if user data mode is admin -> allowed to adding room
        $userData = User::whereNull('deleted_at')->where('id', '=', $request->get('user_id'))->first();

        if (!empty($userData->userType->title) && $userData->userType->title !== 'owner') {
            return response()->json(['success' => 0, 'status' => 'not-allowed', 'message' => 'Not allowed to add rooms']);
        }

        //Make transaction for handling saving data
        $roomId = null;
        DB::beginTransaction();
        try {

            /**
             * Initialize var
             */
            $createdBy = $request->get('user_id');
            $createdAt = date('Y-m-d');

            /**
             * Saving the room data
             */
            $room = new Room();
            $room->owner_id = $request->get('owner_id');
            $room->city_id = $request->get('city_id');
            $room->title = $request->get('title');
            $room->price = $request->get('price');
            $room->created_by = $createdBy;
            $room->created_at = $createdAt;
            $room->save();

            /**
             * Saving the room detail data
             */
            $roomDetail = new RoomDetail();
            $roomDetail->room_id = $room->id;
            $roomDetail->facility = $request->get('facility');
            $roomDetail->description = $request->get('description');
            $roomDetail->available_room = $request->get('available_room');
            $roomDetail->created_by = $createdBy;
            $roomDetail->created_at = $createdAt;
            $roomDetail->save();

            $roomId = $room->id;

            //Commit process
            DB::commit();
        }
        catch (\Exception $ex) {
            //Rollback if any exception occur
            DB::rollback();

            return $this->responseError($ex->getMessage());
        }

        //Show the saved data
        $rooms = Room::whereNull('deleted_at')->where('id', '=', $roomId)->first();

        //insert into elasticsearch stack
        dispatch(new \App\Jobs\ElasticsearchInsertingJob($roomId, $rooms));

        //Output
        return $this->responseOk($rooms);
    }

    /**
     * Update rooms data
     * 
     * @param Request $request
     * @param int $id
     * @return JSON
     */
    public function update(Request $request, $id)
    {
        //Validate input
        $this->validate($request, [
            'owner_id' => 'required|max:11|exists:users,id',
            'city_id' => 'required|max:11|integer|exists:city,id',
            'title' => 'required',
            'price' => 'required',
            'user_id' => 'required|max:11|exists:users,id',
            'facility' => 'required',
            'description' => 'required',
            'rate' => 'required|integer',
        ]);

        //Check if user data mode is admin -> allowed to adding room
        $userData = User::whereNull('deleted_at')->where('id', '=', $request->get('user_id'))->first();

        if (!empty($userData->userType->title) && $userData->userType->title !== 'owner') {
            return response()->json(['success' => 0, 'status' => 'not-allowed', 'message' => 'Not allowed to update rooms']);
        }

        /**
         * Initialize var
         */
        $createdBy = $request->get('user_id');
        $updatedAt = date('Y-m-d H:i:s');
        $roomData = [
            'owner_id' => $request->get('owner_id'),
            'city_id' => $request->get('city_id'),
            'title' => $request->get('title'),
            'price' => $request->get('price'),
            'created_by' => $createdBy,
            'updated_at' => $updatedAt
        ];
        $roomDetailData = [
            'room_id' => $id,
            'facility' => $request->get('facility'),
            'description' => $request->get('description'),
            'rate' => $request->get('rate'),
            'created_by' => $createdBy,
            'updated_at' => $updatedAt
        ];

        //Make transaction for handling saving data

        DB::beginTransaction();
        try {

            /**
             * Updating the room data
             */
            Room::where('id', $id)->whereNull('deleted_at')->update($roomData);

            /**
             * Updating the room detail data
             */
            RoomDetail::whereNull('deleted_at')->where('room_id', $id)->update($roomDetailData);

            //Commit process
            DB::commit();
        }
        catch (\Exception $ex) {
            //Rollback if any exception occur
            DB::rollback();
            return $this->responseError($ex->getMessage());
        }

        //Output
        return $this->responseOk(array_merge($roomDetailData, $roomData));
    }

    /**
     * Soft delete rooms data
     * 
     * @param Request $request
     * @param int $id
     * @return JSON
     */
    public function delete(Request $request, $id)
    {
        //Check if user data mode is admin -> allowed to adding room
        $userData = User::whereNull('deleted_at')->where('id', '=', $request->get('user_id'))->first();

        if (!empty($userData->userType->title) && $userData->userType->title !== 'owner') {
            return response()->json(['success' => 0, 'status' => 'not-allowed', 'message' => 'Not allowed to delete rooms']);
        }

        DB::beginTransaction();
        try {

            $deletedAt = date('Y-m-d H:i:s');

            /**
             * Updating the room data
             */
            Room::where('id', $id)->whereNull('deleted_at')->update(['deleted_at' => $deletedAt]);

            /**
             * Updating the room detail data
             */
            RoomDetail::whereNull('deleted_at')->where('room_id', $id)->update(['deleted_at' => $deletedAt]);

            //Commit process
            DB::commit();
        }
        catch (\Exception $ex) {
            //Rollback if any exception occur
            DB::rollback();
            return $this->responseError($ex->getMessage());
        }

        //Output
        return $this->responseOk('Data has been deleted');
    }

    /**
     * Gettimg all rooms list and paginate
     * 
     * @param Request $request
     * @return JSON
     */
    public function roomList(Request $request)
    {

        //Validate input
        $this->validate($request, [
            'page' => 'required|integer',
            'per_page' => 'required|integer',
            'order_field' => 'in:price,rooms_detail.available_room'
        ]);

        $rooms = Room::whereNull('rooms.deleted_at')
            ->join('rooms_detail', 'rooms_detail.room_id', 'rooms.id')
            ->orderBy($request->get('order_field'), $request->get('order_mode', 'ASC'))
            ->paginate($request->get('per_page'));

        return $this->customPagination($rooms, '', 1, 'success');
    }

    /**
     * Room detail by id
     * 
     * @param int $id
     * @return JSON
     */
    public function roomDetail($id)
    {
        //get data by id 
        $rooms = Room::whereNull('deleted_at')->where('id', $id)->first();
        return $this->responseOk($rooms);
    }

    /**
     * Ask for room availability 
     * 
     * @param Request $request
     * @param int $id
     * @return JSON
     */
    public function ask(Request $request, $id)
    {
        if (empty($id) || trim($id) == null) {
            return $this->responseError('Param id cannot be blank');
        }

        //Validate input
        $this->validate($request, [
            'user_id' => 'required|max:11|exists:users,id'
        ]);

        //Getting rooms and user data
        $rooms = Room::whereNull('deleted_at')->where('id', $id)->first();
        $userData = User::whereNull('deleted_at')->where('id', $request->get('user_id'))->first();

        //Initiate user kind title
        $userKind = '';
        if (!empty($userData->userKind->title)) {
            $userKind = $userData->userKind->title;
        }

        //Get user_credit and user_credit_history
        $userCredit = UserCredit::whereNull('deleted_at')->where('user_id', $request->get('user_id'));
        $userCreditVal = $userCredit->first();

        /**
         * Handle user credit data
         */
        $creditVal = 0;
        if (!empty($userCreditVal->credit_current_val)) {
            $creditVal = $userCreditVal->credit_current_val;
        } else {
            $userCreditObj = new UserCredit();
            $userCreditObj->user_id = $request->get('user_id');
            $userCreditObj->credit_current_val = ($userKind === 'premium') ? env('PREMIUM_CREDIT_VAL') : env('REGULAR_CREDIT_VAL');
            $userCreditObj->created_by = $request->get('user_id');
            $userCreditObj->updated_at = date('Y-m-d H:i:s');
            $userCreditObj->save();
        }

        //update user_credit and user_credit_history
        $currentCreditVal = $creditVal - 5;
        $userCredit->update([
            'credit_current_val' => $currentCreditVal
        ]);

        //Insert into user credit history
        $userCreditHistory = new \App\Model\UserCreditHistory();
        $userCreditHistory->user_id = $request->get('user_id');
        $userCreditHistory->credit_history = 5;
        $userCreditHistory->created_by = 1;
        $userCreditHistory->created_at = date('Y-m-d H:i:s');
        $userCreditHistory->save();

        return $this->responseOk($rooms, 'You action is success. Your point is decreased by 5');
    }

    /**
     * Searching rooms by q word
     * 
     * @param Request $request
     * @return JSON
     */
    public function search(Request $request)
    {
        //Validate input
        $this->validate($request, [
            'q' => 'required',
            'per_page' => 'required|integer',
        ]);

        $q = $request->get('q', '');

        return $this->customPagination(
                Room::whereNull('rooms.deleted_at')
                    ->join('city', 'city.id', 'rooms.city_id')
                    ->where('city.title', 'LIKE', '%' . $q . '%')
                    ->orWhere('rooms.title', 'LIKE', '%' . $q . '%')
                    ->orWhere('rooms.price', 'LIKE', '%' . $q . '%')
                    ->paginate($request->get('per_page')), '&q=' . $q . '', 1, 'success'
        );
    }

    /**
     * Function for reserting point every month
     * 
     * @return void
     */
    public function resetUsersPoint()
    {
        $date = date('d');
        if ($date != '01') {
            print 'No action';
            print PHP_EOL;
            exit(0);
        }

        print 'Reseting user point...' . PHP_EOL;

        $users = User::whereNull('deleted_at')->get();
        if (empty($users)) {
            print 'No data';
            print PHP_EOL;
            exit(0);
        }

        foreach ($users as $user) {
            $userKind = \App\Model\UserKind::select('title')
                    ->whereNull('deleted_at')
                    ->where('id', $user->user_kind_id)->first();

            if (!empty($userKind->title)) {

                if ($userKind->title == 'premium') {
                    print "Reseting premium point..." . PHP_EOL;
                    UserCredit::whereNull('deleted_at')
                        ->where('user_id', $user->id)
                        ->update(['credit_current_val' => env('PREMIUM_CREDIT_VAL')]);
                } else {
                    print "Reseting regular point..." . PHP_EOL;
                    UserCredit::whereNull('deleted_at')
                        ->where('user_id', $user->id)
                        ->update(['credit_current_val' => env('REGULAR_CREDIT_VAL')]);
                }
            }
        }

        print 'Free memory action...';
        print PHP_EOL;
        unset($users);
    }

}

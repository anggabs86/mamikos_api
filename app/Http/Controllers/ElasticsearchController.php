<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Description of ElasticsearchController
 *
 * @author Angga Bayu S<anggabs86@gmail.com>
 */
class ElasticsearchController extends Controller
{

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        parent::__construct();
    }

    /**
     * Install elasticsearch Document
     * 
     * @return JSON
     */
    public function install()
    {
        $client = new \GuzzleHttp\Client();

        $checkDocument = json_encode([]);
        try {
            $checkDocument = $client->get(env('ELASTIC_SEARCH_URL') . '/_count');
        }
        catch (\Exception $ex) {

            $response = json_encode([]);
            try {
                print "Creating Elasticsearch document...";
                print PHP_EOL;
                $response = $client->put(env('ELASTIC_SEARCH_URL') . '');
            }
            catch (\Exception $ex) {
                dd("Exception occured: " . $ex->getMessage());
                print PHP_EOL;
                exit;
            }

            dd("Elasticsearch document has been successfully created");
            print PHP_EOL;
        }
    }

    public function searchResult(Request $request)
    {
        $q = $request->get('q');
        $client = new \App\Helpers\HttpClient();
        $response = $client->getRequest(env('ELASTIC_SEARCH_URL') . '/_search?q=' . "*$q*", json_encode([]));

        header('Content-type: application/json');
        print $response;
        exit(0);
    }

}

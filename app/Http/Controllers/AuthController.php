<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\SecurityHelper;
use App\Model\User;
use App\Model\UserToken;

/**
 * Description of AuthController
 *
 * @author Angga Bayu S<anggabs86@gmail.com>
 */
class AuthController extends Controller
{

    /**
     * Constructor
     * call parent constructor
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get UUID from device
     * 
     * @param Request $request
     * @return JSON
     */
    public function getUUID(Request $request)
    {
        //Validate request
        $this->validate($request, [
            'imei' => 'required',
            'phone_number' => 'required'
        ]);

        //Initialize vars
        $imei = $request->get('imei');
        $phoneNumber = $request->get('phone_number');
        $combinedStr = $imei . '___' . $phoneNumber;
        $publicKey = SecurityHelper::getPublicKey();
        $encryptedValue = SecurityHelper::rsaEncrypt($combinedStr, $publicKey);

        return $this->responseOk($encryptedValue);
    }

    /**
     * Verify UUID from server
     * 
     * @param Request $request
     * @return JSON
     */
    public function verifyUUID(Request $request)
    {
        //Validate request
        $this->validate($request, [
            'imei' => 'required',
            'phone_number' => 'required',
            '_uuid' => 'required'
        ]);

        //Initialiaze vars
        $imei = $request->get('imei');
        $phoneNumber = $request->get('phone_number');
        $uuid = $request->get('_uuid');

        $privateKey = SecurityHelper::getPrivateKey();
        $decryptedUUID = SecurityHelper::rsaDecrypt($uuid, $privateKey);
        $explodeDecryptedData = explode("___", $decryptedUUID);

        //Check if valid UUID
        if (empty($explodeDecryptedData[0]) || empty($explodeDecryptedData[1])) {
            return $this->responseError('Not valid UUID');
        }

        //Check is valid UUID?
        if ($explodeDecryptedData[0] === $request->get('imei') && $explodeDecryptedData[1] === $request->get('phone_number')) {
            return $this->responseOk([
                    'imei' => $imei, 'phone_number' => $phoneNumber, '_uuid' => $uuid
                    ], 1, 'valid uuid'
            );
        }

        return $this->responseError('Not valid UUID');
    }

    /**
     * Login procress
     * 
     * @param Request $request
     * @return JSON
     */
    public function login(Request $request)
    {
        //validate request
        $this->validate($request, [
            'username' => 'required|email',
            'password' => 'required|min:5',
            '_uuid' => 'required',
        ]);

        //Initialize vars
        $uuid = $request->get('_uuid');

        $privateKey = SecurityHelper::getPrivateKey();
        $decryptedUUID = SecurityHelper::rsaDecrypt($uuid, $privateKey);
        $explodeDecryptedData = explode("___", $decryptedUUID);

        //Check if valid UUID
        if (empty($explodeDecryptedData[0]) || empty($explodeDecryptedData[1])) {
            return $this->responseError('Not valid UUID');
        }

        //Get user data based on request
        $users = User::whereNull('deleted_at')
            ->where('email', $request->get('username'))
            ->where('password', sha1($request->get('password')))
            ->first();

        if (empty($users->id)) {
            return $this->responseError('Wrong username and password value');
        }

        $dateNow = date('Y-m-d H:i:s');
        $expiredTokenDate = date('Y-m-d H:i:s', strtotime(env('EXPIRED_TOKEN_DIFF'), strtotime($dateNow)));

        $encryptedToken = encrypt(env('TOKEN_SALT') . '_' . $uuid);
        //Update token and expired ones
        $userToken = UserToken::whereNull('deleted_at')->where('user_id', $users->id);

        if (!empty($userToken->first()->id)) {
            $userToken->update(
                [
                    'token' => $encryptedToken,
                    'expired_in' => $expiredTokenDate,
                ]
            );
        } else {
            $userTokenObj = new UserToken();
            $userTokenObj->user_id = $users->id;
            $userTokenObj->token = $encryptedToken;
            $userTokenObj->uuid = $uuid;
            $userTokenObj->expired_in = $expiredTokenDate;
            $userTokenObj->created_at = $dateNow;
            $userTokenObj->save();
        }

        //Validate user data with _uuid
        if ($users->imei === $explodeDecryptedData[0] && $users->phone_number === $explodeDecryptedData[1]) {
            return $this->responseOk([
                    'user' => $users,
                    'token' => $encryptedToken,
                    'expired_token_in' => $expiredTokenDate], 1, 'login success'
            );
        } else {
            return $this->responseError('Not allowed');
        }
    }

    /**
     * Verifing token 
     * 
     * @param Request $request
     * @return JSON
     */
    public function verifyToken(Request $request)
    {
        //validate request
        $this->validate($request, [
            'token' => 'required',
        ]);

        try {

            $decryptedToken = decrypt($request->get('token'));
            $explodeToken = explode("_", $decryptedToken);

            if (empty($explodeToken[1])) {
                return $this->responseError('Token is invalid');
            }

            $decryptedUUID = SecurityHelper::rsaDecrypt($explodeToken[1], SecurityHelper::getPrivateKey());
            $explodeUUID = explode("___", $decryptedUUID);

            if (empty($explodeUUID)) {
                return $this->responseError("UUID is not valid");
            }

            $imei = (!empty($explodeUUID[0])) ? $explodeUUID[0] : '';
            $phoneNumber = (!empty($explodeUUID[1])) ? $explodeUUID[1] : '';

            if (empty($imei) && (empty($phoneNumber))) {
                return $this->responseError("UUID is not valid data");
            }

            $userData = User::whereNull('deleted_at')
                ->where('imei', $imei)
                ->where('phone_number', $phoneNumber)
                ->first();

            if (empty($userData->id)) {
                return $this->responseError("Token is invalid");
            }

            $tokenExpiredDate = strtotime($userData->userToken->expired_in);
            $dateNow = strtotime(date('Y-m-d H:i:s'));

            if ($dateNow > $tokenExpiredDate) {
                return $this->responseError("Token is expired");
            }

            return $this->responseOk([
                    'user_id' => $userData->id,
                    'name' => $userData->name,
                    'email' => $userData->email,
                    'token' => $userData->userToken->token,
                    ], 1, 'Token is valid');
        }
        catch (\Exception $e) {
            return $this->responseError($e->getMessage());
        }
    }

}

<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    /**
     * Object constructor
     *
     * @return void
     */
    public function __construct()
    {
        // Validation formatter
        self::buildResponseUsing(function ($request, $errors)
        {

            // Format errors
            foreach ($errors as $key => $values) {
                $errors[$key] = $values[0];
            }

            return response()->json([
                    'status' => 0,
                    'message' => 'validation-error',
                    'fields' => $errors,
                    ], 422);
        });
    }

    /**
     * Returning response success/ok from API
     * 
     * @param Object $data
     * @return JSON
     */
    protected function responseOk($data, $success = 1, $status = 'success')
    {
        if (empty($data)) {
            return response()->json(['success' => 0, 'status' => 'empty', 'data' => []], 404);
        }

        return response()->json(['success' => $success, 'status' => $status, 'data' => $data]);
    }

    /**
     * Returning response error from API
     * 
     * @param Object $errors
     * @return JSON
     */
    protected function responseError($errors)
    {

        return response()->json([
                'success' => 0,
                'status' => 'error',
                'error_message' => $errors
                ], 422
        );
    }

    /**
     * custom pagination for API
     *
     * @param Object $objectPagination
     * @param String $paramAttr
     *
     * @return Array
     */
    public static function customPagination($objectPagination, $paramAttr, $status = null, $message = null)
    {

        $oldPAginationArray = $objectPagination->toArray();

        $path = $oldPAginationArray['path'];

        unset($oldPAginationArray['total']);
        unset($oldPAginationArray['per_page']);
        unset($oldPAginationArray['current_page']);
        unset($oldPAginationArray['last_page']);
        unset($oldPAginationArray['per_page_url']);
        unset($oldPAginationArray['first_page_url']);
        unset($oldPAginationArray['last_page_url']);
        unset($oldPAginationArray['next_page_url']);
        unset($oldPAginationArray['prev_page_url']);
        unset($oldPAginationArray['from']);
        unset($oldPAginationArray['path']);
        unset($oldPAginationArray['to']);

        $firstPageUrl = ($objectPagination->url(1) != null) ? $objectPagination->url(1) . $paramAttr : $objectPagination->url(1);
        $nextPageUrl = ($objectPagination->nextPageUrl() != null) ? $objectPagination->nextPageUrl() . $paramAttr : $objectPagination->nextPageUrl();
        $previousPageUrl = ($objectPagination->previousPageUrl() != null) ? $objectPagination->previousPageUrl() . $paramAttr : $objectPagination->previousPageUrl();
        $currentPageUrl = ($objectPagination->url($objectPagination->currentPage()) != null) ? $objectPagination->url($objectPagination->currentPage()) . $paramAttr : $objectPagination->url($objectPagination->currentPage());
        $lastPageUrl = ($objectPagination->lastPage() != null) ? $objectPagination->lastPage() . $paramAttr : $objectPagination->lastPage();

        if (!empty($status) && !empty($message)) {
            $newPagination = array(
                "status" => $status,
                "message" => $message,
                "page_context" => array("total" => $objectPagination->total(),
                    "count" => $objectPagination->count(),
                    "per_page" => (int) $objectPagination->perPage(),
                    "last_item" => $objectPagination->lastItem(),
                    "current_page" => $objectPagination->currentPage(),
                    "first_page_url" => $firstPageUrl,
                    "last_page" => $objectPagination->lastPage(),
                    "first_page_url" => $firstPageUrl,
                    "current_page_url" => $currentPageUrl,
                    "next_page_url" => $nextPageUrl,
                    "last_page_url" => $lastPageUrl,
                    "prev_page_url" => $previousPageUrl,
                    "path" => $path,
                    "has_more_pages" => $objectPagination->hasMorePages()
            ));
        } else {
            $newPagination = array(
                "page_context" => array("total" => $objectPagination->total(),
                    "count" => $objectPagination->count(),
                    "per_page" => (int) $objectPagination->perPage(),
                    "last_item" => $objectPagination->lastItem(),
                    "current_page" => $objectPagination->currentPage(),
                    "first_page_url" => $firstPageUrl,
                    "last_page" => $objectPagination->lastPage(),
                    "current_page_url" => $currentPageUrl,
                    "next_page_url" => $nextPageUrl,
                    "last_page_url" => $lastPageUrl,
                    "prev_page_url" => $previousPageUrl,
                    "path" => $path,
                    "has_more_pages" => $objectPagination->hasMorePages()
            ));
        }

        return array_merge($oldPAginationArray, $newPagination);
    }

}

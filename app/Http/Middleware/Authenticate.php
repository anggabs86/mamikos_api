<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\Model\User;
use App\Helpers\SecurityHelper;

class Authenticate
{

    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        $token = $request->header('token');

        if (empty($token)) {
            return response()->json(['status' => 0, 'message' => 'Missing token'], 422);
        }

        try {

            $decryptedToken = decrypt($token);
            $explodeToken = explode("_", $decryptedToken);

            if (empty($explodeToken[1])) {
                return response()->json(['status' => 0, 'message' => 'Token is invalid'], 422);
            }

            $decryptedUUID = SecurityHelper::rsaDecrypt($explodeToken[1], SecurityHelper::getPrivateKey());
            $explodeUUID = explode("___", $decryptedUUID);

            if (empty($explodeUUID)) {
                return response()->json(['status' => 0, 'message' => 'UUID is not valid'], 422);
            }

            $imei = (!empty($explodeUUID[0])) ? $explodeUUID[0] : '';
            $phoneNumber = (!empty($explodeUUID[1])) ? $explodeUUID[1] : '';

            if (empty($imei) && (empty($phoneNumber))) {
                return response()->json(['status' => 0, 'message' => 'UUID is not valid data'], 422);
            }

            $userData = User::whereNull('deleted_at')
                ->where('imei', $imei)
                ->where('phone_number', $phoneNumber)
                ->first();

            if (empty($userData->id)) {
                return response()->json(['status' => 0, 'message' => 'Token is not valid'], 422);
            }

            if (!empty($userData->userToken->expired_in)) {
                $tokenExpiredDate = strtotime($userData->userToken->expired_in);
                $dateNow = strtotime(date('Y-m-d H:i:s'));
            } else {
                return response()->json(['status' => 0, 'message' => 'Token is invalid'], 422);
            }

            if ($dateNow > $tokenExpiredDate) {
                return response()->json(['status' => 0, 'message' => 'Token is expired'], 422);
            }
        }
        catch (\Exception $e) {
            return response()->json(['status' => 0, 'message' => $e->getMessage()], 500);
        }

        return $next($request);
    }

}

<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Room
 * This class for handling Room Modeling
 *
 * @author Angga Bayu S<anggabs@rocketmail.com>
 */
class Room extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "rooms";

    /**
     * Room has one of
     * @var Array 
     */
    protected $with = ['roomDetail', 'city', 'user'];

    /**
     * Has one relationship to RoomDetail
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function roomDetail()
    {
        return $this->hasOne('App\Model\RoomDetail', 'room_id');
    }

    /**
     * Belongs to relationship to City
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Model\City', 'city_id');
    }

    /**
     * Belongs to relationship to User
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Model\User', 'created_by');
    }

}

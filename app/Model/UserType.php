<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of User
 *
 * @author Angga Bayu S<anggabs@rocketmail.com>
 */
class UserType extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "users_type";

}

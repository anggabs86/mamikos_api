<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of User
 *
 * @author root
 */
class UserDetail extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "users_detail";

}

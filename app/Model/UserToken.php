<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserToken
 *
 * @author root
 */
class UserToken extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "users_token";

}

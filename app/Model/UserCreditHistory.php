<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserCreditHistory
 *
 * @author Angga Bayu S<anggabs86@gmail.com>
 */
class UserCreditHistory extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "users_credit_history";

}

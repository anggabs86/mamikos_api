<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of User
 *
 * @author Angga Bayu S<anggabs86@gmail.com>
 */
class User extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "users";

    /**
     * Room has one of
     * @var Array 
     */
    protected $with = ['userDetail', 'userType', 'userToken', 'userCredit', 'userCreditHistory', 'userKind'];

    /**
     * Has one relationship to UserDetail
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userDetail()
    {
        return $this->hasOne('App\Model\UserDetail', 'user_id');
    }

    /**
     * Belongs to relatiionship to UserType
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userType()
    {
        return $this->belongsTo('App\Model\UserType', 'user_type_id', 'id');
    }

    /**
     * Has one relationship to UserToken
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userToken()
    {
        return $this->hasOne('App\Model\UserToken', 'user_id');
    }

    /**
     * has one relationship to UserCredit
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userCredit()
    {
        return $this->hasOne('App\Model\UserCredit', 'user_id');
    }

    /**
     * Has many relationship to UserCreditHistory
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userCreditHistory()
    {
        return $this->hasMany('App\Model\UserCreditHistory', 'user_id');
    }

    /**
     * Has one relationship to UserKind
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userKind()
    {
        return $this->belongsTo('App\Model\UserKind', 'user_kind_id', 'id');
    }

}

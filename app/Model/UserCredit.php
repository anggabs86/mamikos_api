<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserCredit
 *
 * @author Angga Bayu S<anggabs86@gmail.com>
 */
class UserCredit extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "users_credit";

}

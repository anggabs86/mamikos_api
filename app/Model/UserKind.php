<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of UserKind
 *
 * @author Angga Bayu S<anggabs@rocketmail.com>
 */
class UserKind extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "users_kind";

}
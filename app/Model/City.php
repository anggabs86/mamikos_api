<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of City
 *
 * @author Angga Bayu S<anggabs86@gmail.com>
 */
class City extends Model
{

    /**
     * Property for table name
     * @var String 
     */
    protected $table = "city";

}

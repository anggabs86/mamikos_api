<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers;
use Illuminate\Console\Command;

/**
 * Description of ElasticsearchInstallCommand
 *
 * @author root
 */
class ElasticsearchInstallCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "elasticsearch:install";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Elasticsearch installation";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request)
    {
        $es = new Controllers\ElasticsearchController($request);
        $es->install();
    }

}

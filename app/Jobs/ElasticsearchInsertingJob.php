<?php

namespace App\Jobs;

/**
 * Class ElasticsearchInsertingJob
 * for handling inserting jobs to Elasticsearch stack
 * 
 * @author Angga Bayu S<anggabs86@gmail.com>
 */
class ElasticsearchInsertingJob extends Job
{

    /**
     * Rooms ID
     * @var int 
     */
    public $roomId;

    /**
     * Property for injecting data from other resource
     * 
     * @var Array 
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($roomId, $data = array())
    {
        $this->roomId = $roomId;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $httpClient = new \App\Helpers\HttpClient();
        $result = $httpClient->putRequest(env('ELASTIC_SEARCH_URL').'/'.$this->roomId.'/_created', json_encode($this->data), '');
        
        #$resultArray = \GuzzleHttp\json_decode($result, true);
        #print_r($resultArray);
        
    }

}

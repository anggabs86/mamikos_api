<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->text('title')->nullable();
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
        });

        $data = array(
            array(
                'title' => 'Magelang',
                'created_at' => date('Y-m-d H:i:s')
            ), array(
                'title' => 'Yogyakarta',
                'created_at' => date('Y-m-d H:i:s')
            )
            //...
        );

        // Insert some stuff
        DB::table('city')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city');
    }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersKindTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_kind', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('created_by');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
        });

        $data = array(
            array(
                'id' => 1,
                'title' => 'premium',
                'created_by' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ), array(
                'id' => 2,
                'title' => 'reguler',
                'created_by' => 1,
                'created_at' => date('Y-m-d H:i:s')
            )
            //...
        );

        // Insert some stuff
        DB::table('users_kind')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_kind');
    }

}

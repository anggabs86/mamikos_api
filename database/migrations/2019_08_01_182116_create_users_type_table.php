<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTypeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_type', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');
            $table->integer('created_by')->nullable();
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
        });

        $data = array(
            array(
                'title' => 'owner',
                'created_by' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ), array(
                'title' => 'user',
                'created_by' => 1,
                'created_at' => date('Y-m-d H:i:s')
            )
            //...
        );

        // Insert some stuff
        DB::table('users_type')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_type');
    }

}

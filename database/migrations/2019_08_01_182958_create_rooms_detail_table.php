<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->index();
            $table->integer('available_room');
            $table->text('facility')->nullable();
            $table->text('description')->nullable();
            $table->enum('rate', ['1', '2', '3', '4', '5'])->default('1');
            $table->integer('created_by');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms_detail');
    }
}

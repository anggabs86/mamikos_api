<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_type_id')->index();
            $table->integer('user_kind_id')->index();
            $table->string('name');
            $table->string('email')->unique();
            $table->text('password');
            $table->string('imei');
            $table->string('phone_number');
            $table->enum('status', ['1', '']);
            $table->integer('created_by');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
        });

        $data = array(
            array(
                'id' => 1,
                'user_type_id' => '1',
                'user_kind_id' => '1',
                'name' => 'User Admin',
                'email' => 'test@admin.com',
                'password' => sha1('admin'),
                'imei' => '123',
                'phone_number' => '085601547177',
                'status' => '1',
                'created_by' => '1',
                'created_at' => date('Y-m-d')
            ), array(
                'id' => 2,
                'user_type_id' => '2',
                'user_kind_id' => '2',
                'name' => 'User Ordinary 2',
                'email' => 'test@ordinary.com',
                'password' => sha1('admin'),
                'imei' => '123',
                'phone_number' => '085601547177',
                'status' => '1',
                'created_by' => '1',
                'created_at' => date('Y-m-d')
            )
            //...
        );

        // Insert some stuff
        DB::table('users')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersCreditTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_credit', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->integer('user_id')->index();
            $table->integer('credit_current_val')->nullable();
            $table->integer('created_by')->nullable();
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
        });

        $data = array(
            array(
                'user_id' => '1',
                'credit_current_val' => 40,
                'created_by' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ), array(
                'user_id' => '2',
                'credit_current_val' => 20,
                'created_by' => 1,
                'created_at' => date('Y-m-d H:i:s')
            )
            //...
        );

        // Insert some stuff
        DB::table('users_credit')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_credit');
    }

}

<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$router->group(['middleware' => 'App\Http\Middleware\Authenticate', 'prefix' => 'rooms'], function () use ($router)
{
    $router->post('/create', 'RoomController@add');
    $router->put('/update/{id}', 'RoomController@update');
    $router->delete('/delete/{id}', 'RoomController@delete');
    $router->get('/', 'RoomController@roomList');
    $router->get('/detail/{id}', 'RoomController@roomDetail');
    $router->get('/search', 'RoomController@search');
    $router->get('/es_search', 'ElasticsearchController@searchResult');
    $router->post('/ask/{id}', 'RoomController@ask');
});

$router->group(['prefix' => 'generate'], function () use ($router)
{
    $router->post('/uuid', 'AuthController@getUUID');
});

$router->group(['prefix' => 'auth'], function () use ($router)
{
    $router->post('/verify_uuid', 'AuthController@verifyUUID');
    $router->post('/login', 'AuthController@login');
    $router->post('/verify_token', 'AuthController@verifyToken');
});

$router->group(['middleware' => 'App\Http\Middleware\Authenticate', 'prefix' => 'elasticsearch'], function () use ($router)
{
    $router->post('/install', 'ElasticsearchController@install');
});
